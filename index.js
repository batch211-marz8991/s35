const express = require("express");

// Mongoose is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// connect to the database by passing in our connection string, remember to replace the password and database names with actual values

// SYNTAX
// mongoosr.connect("<MongoDB connection string>",{useNewUrlParser:true})

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin123:admin123@project0.3amnokz.mongodb.net/s35?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);
// SET notifications for connection success or failure
// allow us to handle errors when the initial connection is established
let db = mongoose.connection;

// if a conncetion error occured, output in the console
// allow us to print errors in our terminal
db.on("error",console.error.bind(console,"connection error"));
db.once("open",() => console.log("We're connected to the cloud database"))

// Mongoose Schemas
// dertermine the structure of the documents to be written in the database
// act as blueprints to our data
// use the schema() constructor of the Mongoose module to create a new Schema object
// the "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
// define the fields with their corresponding data type
    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

// Models
// uses Schema and are used to create/instantiate objects that correspond to the schema
// Server>Schema(blueprint)>Database>Collection

const Task = mongoose.model("Task", taskSchema);



// SETUP for allowing the server to handle data from request

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a new task
// Business Logic
/*
    1. Add a functionality to check if there are duplicate task
    -if the tas already exist in the database, we return an error.
    -if the task does not exist in the database, we add it in the database
    2. The taks data will be coming from the request body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided
*/

app.post("/task",(req,res) => {

    Task.findOne({name:req.body.name},(err,result) => {

        if(result != null && result.name == req.body.name){
            return res.send("Duplicate task found")
        } else {
            let newTask = new Task({
                name: req.body.name
            });
            newTask.save((saveErr,savedTask) =>{

                if(saveErr){
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New task Created")
                }
            })
        }
    })
})

//getting all the task
//bussiness logic

app.get("/tasks",(req,res)=>{
	Task.find({}, (err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

//Activity
//create user schema
const userSchema = new mongoose.Schema({
    userName: String,
    password: String
})
const User = mongoose.model("User", userSchema);

//register a user
app.post("/signUp",(req,res)=>{

    User.findOne({userName:req.body.userName},(err,result) => {

        if(result != null && result.userName == req.body.userName){
            return res.send("Duplicate user found")
        }else{
            if (req.body.userName != ''&&
                password: req.body.password !== ''{
                	username : req.body.username,
                	password : req.body.password
                }
            });
            newUser.save((saveErr,savedUser) =>{

                if(saveErr){
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New user registered")
                } else{
                	return res.send("Both username and passwordd must be provided")
                }

            })
        }
    })
})

//retrieving all
app.get("/users",(req,res)=>{
    User.find({},(err,result)=>{
        if(err){
            return console.log(err);
        }else{
            return res.status(200).json({
                data : result
            })
        }
    })
})

app.listen(port,() => console.log(`Server running at port ${port}`))


